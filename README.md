# Cubemos Facial Authentication C++ Client

[[_TOC_]]


## Building
* Init the repository:
```
git submodule update --init
```
### Windows 
* Build with `cmake-vcpkg-build.ps1` ( creates x86 build by default)

### Linux
* Write build script ;-)


## Installation & Integration in your own CMake project
 * Build the library as described above.
 * The library will be installed in `build/install`
 * A Cmake config file can be found in in `build/install/lib/cmake/FaceReidClient`
 * To use this library in your own CMake project set `FaceReidClient_DIR` to the location of the config file and use 
 ```
 find_package(FaceReidClient CONFIG)
 ```
   in your CMakeLists.txt
* A minimal Cmake project using the library can be found in [exampes](./examples)

## Usage of the C++ Client Library
The client library gives access to the three main endpoints of the cubemos facial authentication api as described in the [api documentation](#description-of-the-rest-facial-authentication-api)


There is also a [sample application](./app) which allows you to test the client library with a webcam.

The main entrypoint is the `FaceReidClient`class which is documented in the [header file](./include/facereid_client.hpp).

### Reccomended workflow
1. Define the name of the bucket you want to use to store facial features. Future authentication attempts always compare against facial features stored in a specfic bucket.
```cpp
std::string bucket_id = "test-bucket";
```
2. Get a valid `api-key` from cubemos GmbH to be able to use the API.
```cpp
std::string api_key = "some-key";
```
3. Create an instance of the class `FaceReidClient`.
```cpp
#include<facereidclient/facereid_client.hpp>
using namespace cm;
auto client = FaceReidClient(api_key, bucket_id);
```
4. Learn facial features of all the persons you want to compare against. Do this by taking multiple images and uploading them  by calling `FaceReidClient::teach_in`. It is recommended to take multiple images per person with different angles and lightning conditions.
```cpp
// take an image from webcam or load it from the filesystem
cv::Mat img; // = ...;

// Name or other id of the person you want to learn in
std::string person_id = "Max Mustermann"; 

//detailed response from server. Can be investigated in case of failure.
std::string http_response; 

int http_status_code =  client.teach_in(img, person_id, http_response);
```

5. Once all persons are known to the system you can identify unknown persons by calling ` FaceReidClient::identify`

```cpp
// take an image from webcam or load it from the filesystem
cv::Mat unknown_img; // = ...;

// Output parameter of "identify"
std::string person_id;

//detailed response from server. 
std::string http_response;

int http_status_code =  client.identify(unknown_img, person_id, http_response);
```

## Usage of the C++ Client Application
There is a test application bundled with the library. You can find the documentation [here](./app/).

# Description of the REST Facial Authentication API

cubemos Facial Authentication API for POS.

The API makes automated authentication based on facial biometric information possible. 

Send an image to the API. Retrieve the name of the person.

The API provides teach-in routine for new employees/customers/etc. and also routines to check if the person (e.g. in front of the counter) is one of the employees/customers or not.

1) Create a bucket for your Shop using /create-bucket where all your biometric information are assigned to
2) Teach-in new people into a specific bucket using based on their facial information using /teach-in
3) Authenticate people within a specific bucket using /identify 


## Create Bucket - POST
### Path
`https://api.cubemos.com/facialauth/create-bucket`

### Json-Input
```json
{
  "id": "string"
}
```


### Description
Before you start you have to create at least one bucket.
Within a bucket the biometric meta information located.

The biometric comparison of two persons/images always takes place within a specific bucket.

Example Use Case:  
Pharmacies want to secure their POS systems. Only employees should be able to login into the system. Fruther the POS system should automatically know who is working with the system. In this case one would create one bucket for each pharmacy.


## Teach in - POST
### Path
`https://api.cubemos.com/facialauth/teach-in`

### Json Input
```json
{
  "bucket_id": "string",
  "image": "string",
  "person_id": "string",
}
```

### Description
In order to identify a person, you first need to set up a database of facial features for a number of persons. All future calls to [identify](#identify-post) will try to find a match to one of the persons in the database.  
To populate the database, you can upload images of a person to a specific bucket with `teach-in`.  

It is important that you always use the same identifier (e.g name, email adress, etc) for a specific person. 

We recommend uploading multiple images per persons under different angles and lightning conditition for the best identification performance.

## Identify - POST
### Path
`https://api.cubemos.com/facialauth/identify`

### Json Input
```json
{
  "bucket_id": "string",
  "image": "string
}
```

### Json Output
```json
{
  "person_id": "string"
}
```

### Description
Identifying unknown persons is the core feature of the cubemos facial authentication api. You simply give in an image of a person and the id of the bucket to be searched and you will recieve the id of the matched person or an empty string if no positive match was found.

