.\vcpkg\bootstrap-vcpkg.bat
.\vcpkg\vcpkg install opencv4[png] spdlog nlohmann-json cxxopts curl[winssl] --triplet x86-windows

if ( Test-Path -Path '.\build' -PathType Container ) {
} else {
   mkdir build
}


Set-Location build

cmake -G"Visual Studio 15 2017" "-DCMAKE_INSTALL_PREFIX=.\install" -DVCPKG_TARGET_TRIPLET=x86-windows  -DBUILD_SHARED_LIBS=TRUE "-DCMAKE_TOOLCHAIN_FILE=..\vcpkg\scripts\buildsystems\vcpkg.cmake" ..
cmake --build . --config Release --target install

Set-Location ..