
cmake_minimum_required(VERSION 3.10)

project(libfacereidclient)

option(BUILD_SAMPLE "Build a webcam gui sample to show the use of the library" ON)

add_subdirectory(src)
if (BUILD_SAMPLE)
    add_subdirectory(app)
endif()


install(EXPORT libfacereidclient_target
        FILE
          FaceReidClientTargets.cmake
        NAMESPACE
          FaceReidClient::
        DESTINATION
          lib/cmake/FaceReidClient
      )


include(CMakePackageConfigHelpers)

configure_package_config_file(FaceReidClientConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/FaceReidClientConfig.cmake
  INSTALL_DESTINATION lib/cmake/FaceReidClient
  )

write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/FaceReidClientConfigVersion.cmake
  VERSION 1.1.0
  COMPATIBILITY SameMajorVersion )

  install(FILES
  "${CMAKE_CURRENT_BINARY_DIR}/FaceReidClientConfig.cmake"
  "${CMAKE_CURRENT_BINARY_DIR}/FaceReidClientConfigVersion.cmake"
  DESTINATION lib/cmake/FaceReidClient)

