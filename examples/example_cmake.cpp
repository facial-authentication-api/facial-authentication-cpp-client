#include <opencv2/imgcodecs.hpp>
#include <iostream>
#include <string>

#include <facereidclient/facereid_client.hpp>

int main(int argc, char* argv[]){
    std::string image_path("image_path");
    std::string api_key("api_key");
    std::string bucket_id("bucket_id");

    cv::Mat img = cv::imread(image_path);

    cm::FaceReidClient client(api_key, bucket_id);
    
    client.create_bucket(bucket_id);

    std::string response;
    int status_code = client.teach_in(img, "person", response);
    


}