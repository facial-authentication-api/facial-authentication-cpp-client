cmake_minimum_required(VERSION 3.10)
project(FaceReidCmakeExample LANGUAGES CXX)

find_package(OpenCV 4.1.1 REQUIRED COMPONENTS imgcodecs core CONFIG)
find_package(FaceReidClient REQUIRED CONFIG)


add_executable(ClientExampleCmake teach_image.cpp)
target_link_libraries(ClientExampleCmake PRIVATE ${OpenCV_LIBS} FaceReidClient::FaceReidClient)

