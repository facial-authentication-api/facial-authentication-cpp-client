# Changelog

## Master
 * Server Url can now be changed by setting the environment variable `CUBEMOS_FaceReid_Server_URI` to the desired url. If no environment variable is set the standard url is used.