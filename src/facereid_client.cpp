#include "../include/facereid_client.hpp"
#include <nlohmann/json.hpp>
#include <opencv2/imgcodecs.hpp>
#include "base64.hpp"

static std::string encode_image(cv::Mat const & image){
    std::vector<uchar> bytestream;
    cv::imencode(".png", image, bytestream);
    return base64::encode(bytestream);
}

namespace cm {
    FaceReidClient::FaceReidClient(std::string api_key, std::string bucket_id): _api_key(api_key), _bucket_id(bucket_id), _url(std::string("https://api.cubemos.com/facialauth/")) {
		char* server_uri = std::getenv("CUBEMOS_FaceReid_Server_URI");
		if (server_uri != nullptr) {
			_url = std::string(server_uri);
		}
	}
    int FaceReidClient::create_bucket(std::string& response){
        std::string url = _url + "create_bucket";
        nlohmann::json j;
        j["id"] = _bucket_id;
        int status_code = request.make_post_request(url, _api_key, j.dump(), response);
        return status_code;    
    };

    int FaceReidClient::teach_in(cv::Mat const & img, std::string person_id, std::string& response){
        
        std::string url = _url + "teach-in";
        nlohmann::json j;
        j["bucket_id"] = _bucket_id;
        j["person_id"] = person_id;
        j["image"] = encode_image(img);
        int status_code = request.make_post_request(url, _api_key, j.dump(), response);
        return status_code;    
    };
    int FaceReidClient::identify(cv::Mat const & img, std::string& person_id, std::string& response){
        std::string url = _url + "identify";
        nlohmann::json j;
        j["bucket_id"] = _bucket_id;
        j["image"] = encode_image(img);
        int status_code = request.make_post_request(url, _api_key, j.dump(), response);        
        if (status_code == 200){
            nlohmann::json j;
            try {
                j = nlohmann::json::parse(response);
				person_id = j["person_id"];
            }
            catch (nlohmann::json::parse_error& er) {
                std::stringstream ss;
                ss << "Unable to parse server response: \"" << response << "\" as json. Details: \"" << er.what()
                   << "\"";
                throw std::runtime_error(ss.str().c_str());
			}
        }
        else{
            person_id = "";
        }
        return status_code;
    }
}