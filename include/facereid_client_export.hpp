#ifdef FACEREIDCLIENT_EXPORTS
#ifdef _WIN32
#define FACEREIDCLIENT_API __declspec(dllexport)
#elif __GNUC__
#define FACEREIDCLIENT_API __attribute__((visibility("default")))
#endif
#else
#ifdef _WIN32
#define FACEREIDCLIENT_API __declspec(dllimport)
#elif __GNUC__
#define FACEREIDCLIENT_API __attribute__((visibility("default")))
#endif
#endif