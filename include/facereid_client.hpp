#pragma once
#include <opencv2/core.hpp>
#include <string>


#include "curl_https_request.hpp"
#include "facereid_client_export.hpp"

namespace cm {
/**
* Cubems Face Reidentification API client. All routes described in the api description have a corresponding public method here.
*/
class FACEREIDCLIENT_API FaceReidClient {
  private:
    std::string _api_key;
    std::string _bucket_id;
    CurlHTTPSRequest request;
    std::string _url;
  public:
    /**
     * Constructor of the Client
     * @param api_key A valid api key provided by cubemos GmbH. Contact info@cubemos.com if you do not posses one.
     * @param bucket_id An identifier of the bucket used for the client instance. 
     */
    FaceReidClient(std::string api_key, std::string bucket_id);


	/**
     * create a new bucket
	 * @param respose The output response returned from the server. Can be investigated in case the return code is not 200(success)
     * @return the http status code returned by the server
     * one.
     */
    int create_bucket(std::string& response);

	/**
     * Teach in a new face.
	 * This route calculates an embedding of a detected face and stores the embedding in the bucket
	 * @param img Opencv image that will be transferred to the server
     * @param person_id The identifer of the person you want to assign to the transmitted image.
     * @param respose The output response returned from the server. Can be investigated in case the return
     * code is not 200(success)
     * @return the http status code returned by the server
     * one.
     */
    int teach_in(cv::Mat const& img, std::string person_id, std::string& response);

	/**
     * Identifies a detected face
     * This route calculates an embedding of a detected face and compares it against the embeddings stored in the bucket
     * @param img Opencv image that will be transferred to the server
     * @param person_id the returned person identifier. Is empty if no match could be found. 
     * @param respose The output response returned from the server. Can be investigated in case the return
     * code is not 200(success)
     * @return the http status code returned by the server
     * one.
     */
    int identify(cv::Mat const& img, std::string& person_id, std::string& response);
};

}