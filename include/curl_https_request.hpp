#include <string>
#include "facereid_client_export.hpp"

namespace cm
{
    class  FACEREIDCLIENT_API CurlHTTPSRequest{

    public:
     CurlHTTPSRequest(void);
     ~CurlHTTPSRequest(void);
     int make_post_request(std::string const & url, std::string const & api_key, std::string const & json_data, std::string& response_data);
    
    };
} // namespace cm
