#include <opencv2/opencv.hpp>
#include <cxxopts.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <facereid_client.hpp>

void init_spdlog(){
    auto console_logger = spdlog::stdout_color_mt("console");    
    spdlog::set_default_logger(console_logger);
}

void log_response(int status_code, std::string const& response){
    if (status_code == 200){
        spdlog::info(response);
    }
    else{
        spdlog::warn("Operation failed with status code: {}. Details: \"{}\"",status_code, response );
    }
}

int main(int argc, char** argv)
{
    cxxopts::Options options("facereid_client", "Face Reidentification Client Sample to show the use of the client library");
    options.add_options()
    ("b,bucket", "Bucket ID where the teached persons are stored", cxxopts::value<std::string>())
    ("p,person", "person ID used when teach is called", cxxopts::value<std::string>())
    ("a,api-key", "api-key to be used", cxxopts::value<std::string>())
    ("c,camera", "Index of the used webcam", cxxopts::value<int>())
    ("h,help", "print usage");

    auto result = options.parse(argc, argv);
    if (result.count("help"))
    {
      std::cout << options.help() << std::endl;
      exit(0);
    }

    int camera_index = 0;
    std::string bucket_id("TEST");    
    if (result.count("camera"))
        camera_index = result["camera"].as<int>();
    if (result.count("bucket"))
        bucket_id = result["bucket"].as<std::string>();
    
    if (result.count("api-key") == 0){
        spdlog::error("Please specify an api-key");
        exit(1);
    }

    std::string person_id{""};
    if (result.count("person")){
        person_id = result["person"].as<std::string>();
    }

    std::string api_key = result["api-key"].as<std::string>();

    cv::VideoCapture cap;
    // open the default camera, use something different from 0 otherwise;
    // Check VideoCapture documentation.
    if(!cap.open(camera_index))
        return 0;

    cm::FaceReidClient client(api_key, bucket_id);

    cv::String win_name = "Face Reidentification client";
    win_name = win_name + " " + bucket_id + " bucket";
    cv::namedWindow(win_name);    
    for(;;)
    {
          cv::Mat frame;
          cap >> frame;
          if( frame.empty() ) break; // end of video stream
          imshow(win_name, frame);
          int key_pressed = cv::waitKey(10);
          try {
			  if (key_pressed == 't'){
				  if (person_id == ""){
					  spdlog::error("No Person identifier specifed. Please restart the application with the --person parameter");
					  continue;
				  }
				  std::string api_response;
				  int status_code = client.teach_in(frame, person_id, api_response);
				  log_response(status_code, api_response);

			  }
			  if (key_pressed == 'i'){
				  std::string api_response;
				  std::string identified_person;
				  int status_code = client.identify(frame, identified_person, api_response);
				  log_response(status_code, api_response);
			  }
          }
          catch (std::exception& ex) {
              spdlog::error("Exception occured. Details \"{}\"", ex.what());
		  }


          if (key_pressed == 27)// stop capturing by pressing ESC 
            break;
    }
    // the camera will be closed automatically upon exit
    // cap.close();
    return 0;
}