# Face Reidentification Client Application

The purpose of this application is th show the functionality of the library. It shows a livestream of a connected webcam and allows to call `FaceReidClient::teach_in` and `FaceReidClient::identify` through button press events.

For the general workflow please see the main [readme](../README.md)

## Usage
Starting the application with `-h` or `--help` shows you the available command line options. 
```
Face Reidentification Client Sample to show the use of the client library
Usage:
  facereid_client [OPTION...]

  -b, --bucket arg   Bucket ID where the teached persons are stored
  -p, --person arg   person ID used when teach is called
  -a, --api-key arg  api-key to be used
  -c, --camera arg   Index of the used webcam. Default is 0. You can also 
                     specify 1,2,3, etc if you have multiple cameras attached.
  -h, --help         print usage
```
* You can teach in a new person by pressing `t` while the application is running. You can see the success of the operation in the log window.

* You can identify an unknown person by pressing `i` while the application is running. You can see in the log window which person was identified. Please make sure to first teach-in multiple persons. Otherwise the api has no images to compare against. 

### How to use it:
1) Start the application with the default system webcamera,  with bucket "store1", person_id "person-name1" and api-key "xyz123"
``` 
.\FaceReidClientApp.exe -a "xyz123" -p "person-name1" -b "store1"  
``` 
2) Do not forget to exchange the API key (contact meet@cubemos.com to get one)
3) Let a person (or you) look into your system webcamera and press t
4) Take more images while slightly moving the head and by pressing t several times.
5) To authenticate a person, let a person watch into the camera and press i
6) See in the log windows which person was identified
7) To teach-in a new person restart the application
``` 
.\FaceReidClientApp.exe -a "xyz123" -p "person-name2" -b "store1"  
``` 






